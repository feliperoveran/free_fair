## Teste desenvolvido utilizando Ruby on Rails v5.0.0.1 e Ruby 2.3.0. ##


* ### Instalação ###

Para instalar a aplicação é necessário que seja instalado o Rails 5 e o banco de dados PostreSQL.

Em seguida, executar os comandos: 
````
bundle install 
rails db:create
rails db:migrate
````



* ### Execução dos testes ###
Os testes foram feitos com o framework RSpec.
Para executar os testes:

````
RAILS_ENV=test rails db:create 
RAILS_ENV=test rails db:migrate 
bundle exec rspec
````


* ### Executar a aplicação ###

````rails server````



* ### Disponibilidade online ###

O aplicativo está hospedado no Heroku, disponível online através do endereço:
https://feliperoveranfreefair.herokuapp.com

Os scripts de teste da API são disparados contra este endereço.


* ### Arquivo de importação ###

A importação das informações é feita através da planilha "DEINFO_AB_FEIRASLIVRES_2014.csv" localizada na pasta /lib/seeds.rb

![Picture1.png](https://bitbucket.org/repo/oG9rLE/images/3612354082-Picture1.png)

A importação pode ser feita utilizando o comando: 
````
rails db:seed
````

* ### Banco de Dados ###

Utilizando o banco de dados relacional PostgreSQL.

O banco de dados possui uma tabela (market_places), que armazena as feiras livres importadas.

![database schema.png](https://bitbucket.org/repo/oG9rLE/images/3552262517-database%20schema.png)

* ### Cobertura de testes ###

Os testes estão localizados na pasta /spec e foram feitos utilizando o framework de testes RSpec (http://rspec.info/).

Os testes da API podem ser encontrados no arquivo /spec/controllers/api/market_places_controller_spec.rb

A informação da cobertura de testes é gerada pela gem **simplecov**, disponível no endereço: https://github.com/colszowka/simplecov.

Os relatório da cobertura pode ser encontrada na pasta /coverage/index.html, na raiz do projeto.

![test_coverage1.png](https://bitbucket.org/repo/oG9rLE/images/1007305135-test_coverage1.png)

![test_coverage2.png](https://bitbucket.org/repo/oG9rLE/images/3490473675-test_coverage2.png)

* ### Geração de logs estruturados em arquivo texto ###

A geração dos logs é feita pela gem **rails_semantic_logger**, disponível no endereço:
https://github.com/rocketjob/rails_semantic_logger.

Esta gem formata os logs nativos do Rails e os converte para o formato JSON.

Os logs são gravados em um arquivo texto localizado na pasta /log. Para o ambiente de desenvolvimento, é gerado o arquivo /log/development.log e para o ambiente de produção, o arquivo /log/production.log.

![logs.png](https://bitbucket.org/repo/oG9rLE/images/381717492-logs.png)


# Scripts para teste da API #

## **INDEX** ##

### Busca de feiras por pelo menos um dos seguintes parâmetros: distrito, regiao5, nome_feira e bairro. ###


* Utilizando todos os parâmetros suportados

Script:
````
curl --request GET \
--url http://feliperoveranfreefair.herokuapp.com/api/market_places \
-d market_place[distrito]="VILA FORMOSA" \
-d market_place[regiao5]=Leste \
-d market_place[nome_feira]="VILA FORMOSA" \
-d market_place[bairro]="VL FORMOSA"
````

Resposta:
````
[{"id":1,"long":-46550164,"lat":-23558733,"setcens":"355030885000091","areap":"3550308005040","coddist":87,"distrito":"VILA FORMOSA","codsubpref":26,"subprefe":"ARICANDUVA-FORMOSA-CARRAO","regiao5":"Leste","regiao8":"Leste 1","nome_feira":"VILA FORMOSA","registro":"4041-0","logradouro":"RUA MARAGOJIPE","numero":"S/N","bairro":"VL FORMOSA","referencia":"TV RUA PRETORIA","created_at":"2017-01-15T13:38:49.597Z","updated_at":"2017-01-15T13:38:49.597Z"},{"id":362,"long":-46534859,"lat":-23562772,"setcens":"355030885000038","areap":"3550308005040","coddist":87,"distrito":"VILA FORMOSA","codsubpref":26,"subprefe":"ARICANDUVA-FORMOSA-CARRAO","regiao5":"Leste","regiao8":"Leste 1","nome_feira":"VILA FORMOSA","registro":"1030-8","logradouro":"AV TRUMAIN C/ HENRIQUE MORIZE","numero":"S/N","bairro":"VL FORMOSA","referencia":"PRACA LIBERIA","created_at":"2017-01-15T13:38:52.438Z","updated_at":"2017-01-15T13:38:52.438Z"}]
````


* Utilizando apenas um dos parâmetros suportados

Script:
````
curl --request GET \
--url http://feliperoveranfreefair.herokuapp.com/api/market_places \
-d market_place[nome_feira]="VILA OLIVIERI"
````

Resposta:
````
[{"id":14,"long":-46594968,"lat":-23645941,"setcens":"355030868000172","areap":"3550308005090","coddist":69,"distrito":"SACOMA","codsubpref":13,"subprefe":"IPIRANGA","regiao5":"Sul","regiao8":"Sul 1","nome_feira":"VILA OLIVIERI","registro":"6061-5","logradouro":"RUA CLAUDIO FERREIRA MANOEL","numero":"29.000000","bairro":"VL OLIVIERI","referencia":"RUA CARLA LIVIERO","created_at":"2017-01-15T13:38:49.693Z","updated_at":"2017-01-15T13:38:49.693Z"}]
````


* Quando não foram encontradas feiras com os parâmetros fornecidos

Script:
````
curl --request GET \
--url http://feliperoveranfreefair.herokuapp.com/api/market_places \
-d market_place[nome_feira]="feira que não existe"
````

Resposta:
````
[]
````

* Quando não foram fornecidos parâmetros de busca válidos 

Script:
````
curl --request GET \
--url http://feliperoveranfreefair.herokuapp.com/api/market_places
````

Resposta:
````
{"error":"no valid search parameters have been provided"}
````


## **CREATE** ##

### Faz a criação de uma feira. Os parâmetros bairro, número e referência são opcionais (há registros na planilha cujo bairro está em branco). ###


* Feira criada com sucesso

Script:
````
curl --request POST \
--url http://feliperoveranfreefair.herokuapp.com/api/market_places \
-d market_place[id]=999 \
-d market_place[long]=-46551234 \
-d market_place[lat]=-23238733 \
-d market_place[setcens]=355030885000091 \
-d market_place[areap]=3550308005040 \
-d market_place[coddist]=87 \
-d market_place[distrito]="VILA FORMOSA" \
-d market_place[codsubpref]=26 \
-d market_place[subprefe]="ARICANDUVA-FORMOSA-CARRAO" \
-d market_place[regiao5]=Leste \
-d market_place[regiao8]="Leste 1" \
-d market_place[nome_feira]="VILA FORMOSA" \
-d market_place[registro]="4041-0" \
-d market_place[logradouro]="RUA MARAGOJIPE" \
-d market_place[numero]="S/N" \
-d market_place[bairro]="VL FORMOSA" \
-d market_place[referencia]="TV RUA PRETORIA"
````

Resposta:
````
{"id":999,"long":-46551234,"lat":-23238733,"setcens":"355030885000091","areap":"3550308005040","coddist":87,"distrito":"VILA FORMOSA","codsubpref":26,"subprefe":"ARICANDUVA-FORMOSA-CARRAO","regiao5":"Leste","regiao8":"Leste 1","nome_feira":"VILA FORMOSA","registro":"4041-0","logradouro":"RUA MARAGOJIPE","numero":"S/N","bairro":"VL FORMOSA","referencia":"TV RUA PRETORIA","created_at":"2017-01-15T14:08:45.237Z","updated_at":"2017-01-15T14:08:45.237Z"}
````


* Erro ao criar a feira

Script:
````
curl --request POST \
--url http://feliperoveranfreefair.herokuapp.com/api/market_places \
-d market_place[id]=999 \
-d market_place[long]=-46551234 \
-d market_place[lat]=-23238733 \
-d market_place[setcens]=355030885000091 \
-d market_place[areap]=3550308005040 \
-d market_place[coddist]=87 \
-d market_place[distrito]="VILA FORMOSA" \
-d market_place[codsubpref]=26 \
-d market_place[subprefe]="ARICANDUVA-FORMOSA-CARRAO" \
-d market_place[regiao5]=Leste \
-d market_place[regiao8]="Leste 1"
````

Resposta:
````
{"nome_feira":["não pode ficar em branco"],"registro":["não pode ficar em branco"],"logradouro":["não pode ficar em branco"]}
````


* Já existe uma feira com o código de registro (ID) fornecido

Script:
````
curl --request POST \
--url http://feliperoveranfreefair.herokuapp.com/api/market_places \
-d market_place[id]=1 \
-d market_place[long]=-46551234 \
-d market_place[lat]=-23238733 \
-d market_place[setcens]=355030885000091 \
-d market_place[areap]=3550308005040 \
-d market_place[coddist]=87 \
-d market_place[distrito]="VILA FORMOSA" \
-d market_place[codsubpref]=26 \
-d market_place[subprefe]="ARICANDUVA-FORMOSA-CARRAO" \
-d market_place[regiao5]=Leste \
-d market_place[regiao8]="Leste 1" \
-d market_place[nome_feira]="VILA FORMOSA" \
-d market_place[registro]="4041-0" \
-d market_place[logradouro]="RUA MARAGOJIPE" \
-d market_place[numero]="S/N" \
-d market_place[bairro]="VL FORMOSA" \
-d market_place[referencia]="TV RUA PRETORIA"
````

Resposta:
````
{"error":"there is already a market place with id 1"}
````

## **UPDATE** ##

### Faz a atualização de uma feira. Todos os parâmetros podem ser atualizados, exceto o código de registro (id). ###


* Atualização com sucesso

Script:
````
curl --request PATCH \
--url http://feliperoveranfreefair.herokuapp.com/api/market_places/2 \
-d market_place[regiao5]=Leste \
-d market_place[distrito]="IPIRANGA" \
-d market_place[id]=123
````

OBS: O id não foi alterado.

Resposta:
````
{"id":2,"distrito":"IPIRANGA","regiao5":"Leste","long":-46574716,"lat":-23584852,"setcens":"355030893000035","areap":"3550308005042","coddist":95,"codsubpref":29,"subprefe":"VILA PRUDENTE","regiao8":"Leste 1","nome_feira":"PRACA SANTA HELENA","registro":"4045-2","logradouro":"RUA JOSE DOS REIS","numero":"909.000000","bairro":"VL ZELINA","referencia":"RUA OLIVEIRA GOUVEIA","created_at":"2017-01-15T13:38:49.607Z","updated_at":"2017-01-15T14:18:54.539Z"}
````


* Erro ao atualizar

Script:
````
curl --request PATCH \
--url http://feliperoveranfreefair.herokuapp.com/api/market_places/2 \
-d market_place[regiao5]="" \
-d market_place[distrito]="IPIRANGA"
````

Resposta:
````
{"regiao5":["não pode ficar em branco"]}
````


* Feira não encontrada

Script:
````
curl --request PATCH \
--url http://feliperoveranfreefair.herokuapp.com/api/market_places/98888 \
-d market_place[regiao5]="" \
-d market_place[distrito]="IPIRANGA"
````

Resposta:
````
{"error":"market_place wasn't found"}
````

## **DESTROY**##
### Exclusão de uma feira através de seu código de registro ###


* Feira excluída com sucesso

Script:
````
curl --request DELETE \
--url http://feliperoveranfreefair.herokuapp.com/api/market_places/2
````

Resposta:
````
{"message":"market_place deleted."}
````


* Feira não encontrada

Script:
````
curl --request DELETE \
--url http://feliperoveranfreefair.herokuapp.com/api/market_places/981838
````

Resposta:
````
{"error":"market_place wasn't found"}
````