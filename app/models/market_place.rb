class MarketPlace < ApplicationRecord
  # :bairro should also be present, but on the CSV file there is a record without it
  validates :long, :lat, :setcens, :areap, :coddist, :distrito, :codsubpref, :subprefe, :regiao5, :regiao8, :nome_feira, :registro, :logradouro, presence: true
end
