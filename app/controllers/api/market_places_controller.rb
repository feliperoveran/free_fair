class Api::MarketPlacesController < ApplicationController
  rescue_from ActiveRecord::RecordNotFound, with: :record_not_found
  rescue_from ActiveRecord::RecordNotUnique, with: :duplicated_record
  skip_before_action :verify_authenticity_token
  before_action :set_market_place, only: [:update, :destroy]

  # GET /api/market_places
  # supported search parameters: distrito, regiao5, nome_feira, bairro
  def index
    # renders and error and return unless valid search parameters have been provided
    if market_place_search_params.blank?
      render json: { error: 'no valid search parameters have been provided' }, status: :forbidden and return
    end

    market_places = MarketPlace.where(market_place_search_params)

    render json: market_places, status: :ok
  end

  # POST /api/market_places
  # required parameters are: :id, :long, :lat, :setcens, :areap, :coddist, :distrito, :codsubpref, :subprefe, :regiao5, :regiao8, :nome_feira, :registro, :logradouro
  # :bairro, :numero, :referencia are optional parameters
  def create
    market_place = MarketPlace.new(market_place_params)

    if market_place.save
      render json: market_place, status: :created
    else
      render json: market_place.errors, status: :unprocessable_entity
    end
  end

  # PATCH /api/market_places/:id
  # PUT /api/market_places/:id
  # supported update parameters: :long, :lat, :setcens, :areap, :coddist, :distrito, :codsubpref, :subprefe, :regiao5, :regiao8, :nome_feira, :registro, :logradouro, :numero, :bairro, :referencia
  # updates every parameter except :id
  def update
    if @market_place.update(market_place_update_params)
      render json: @market_place, status: :ok
    else
      render json: @market_place.errors, status: :unprocessable_entity
    end
  end

  # DELETE /api/market_places/:id
  def destroy
    if @market_place.destroy
      render json: { message: 'market_place deleted.' }, status: :ok
    else
      render json: @market_place.errors, status: :unprocessable_entity
    end
  end

  private
    def set_market_place
      @market_place = MarketPlace.find(params[:id])
    end

    def market_place_params
      params.fetch(:market_place, {}).permit(:id, :long, :lat, :setcens, :areap, :coddist, :distrito, :codsubpref, :subprefe, :regiao5, :regiao8, :nome_feira, :registro, :logradouro, :numero, :bairro, :referencia)
    end

    def market_place_search_params
      params.fetch(:market_place, {}).permit(:distrito, :regiao5, :nome_feira, :bairro)
    end

    def market_place_update_params
      market_place_params.except(:id)
    end

    def record_not_found
      render json: { error: "market_place wasn't found" }, status: :not_found
    end

    def duplicated_record
      render json: { error: "there is already a market place with id #{market_place_params[:id]}" }, status: :forbidden
    end
end
