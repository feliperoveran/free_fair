require 'rails_helper'

RSpec.describe MarketPlace, "validations" do
  it { should validate_presence_of(:long) }
  it { should validate_presence_of(:lat) }
  it { should validate_presence_of(:setcens) }
  it { should validate_presence_of(:areap) }
  it { should validate_presence_of(:coddist) }
  it { should validate_presence_of(:distrito) }
  it { should validate_presence_of(:codsubpref) }
  it { should validate_presence_of(:subprefe) }
  it { should validate_presence_of(:regiao5) }
  it { should validate_presence_of(:regiao8) }
  it { should validate_presence_of(:nome_feira) }
  it { should validate_presence_of(:registro) }
  it { should validate_presence_of(:logradouro) }
end
