FactoryGirl.define do
  factory :market_place do
    long 1
    lat 1
    setcens "MyText"
    areap "MyText"
    coddist 1
    distrito "MyText"
    codsubpref 1
    subprefe "MyText"
    regiao5 "MyText"
    regiao8 "MyText"
    nome_feira "MyText"
    registro "MyText"
    logradouro "MyText"
    numero "MyText"
    bairro "MyText"
    referencia "MyText"
  end
end
