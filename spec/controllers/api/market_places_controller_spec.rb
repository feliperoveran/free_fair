require 'rails_helper'

#####################################
##### testing search parameters #####
#####################################

RSpec.describe Api::MarketPlacesController, "index" do
  # with no parameters
  it "should return an error when no parameter is provided" do
    get :index

    json = JSON.parse(response.body)

    expect(response).to have_http_status :forbidden

    expect(json.has_key?('error')).to eq true
  end

  # :distrito
  it "returns the market_places that have :distrito equals to the provided parameter" do
    market_place = FactoryGirl.create(:market_place, distrito: "VILA MARIANA")
    other_market_place = FactoryGirl.create(:market_place, distrito: "VILA PRUDENTE")

    search_parameters = { market_place: { distrito: "VILA MARIANA" } }

    get :index, params: search_parameters

    json = JSON.parse(response.body)

    expect(response.body).to include market_place.to_json
    expect(response.body).to_not include other_market_place.to_json
  end

  # :regiao5
  it "returns the market_places that have :regiao5 equals to the provided parameter" do
    market_place = FactoryGirl.create(:market_place, regiao5: "Leste")
    other_market_place = FactoryGirl.create(:market_place, regiao5: "Sul")

    search_parameters = { market_place: { regiao5: "Leste" } }

    get :index, params: search_parameters

    json = JSON.parse(response.body)

    expect(response.body).to include market_place.to_json
    expect(response.body).to_not include other_market_place.to_json
  end

  # nome_feira
  it "returns the market_places that have :nome_feira equals to the provided parameter" do
    market_place = FactoryGirl.create(:market_place, nome_feira: "Feira 1")
    other_market_place = FactoryGirl.create(:market_place, nome_feira: "Feira 2")

    search_parameters = { market_place: { nome_feira: "Feira 1" } }

    get :index, params: search_parameters

    json = JSON.parse(response.body)

    expect(response.body).to include market_place.to_json
    expect(response.body).to_not include other_market_place.to_json
  end

  # bairro
  it "returns the market_places that have :bairro equals to the provided parameter" do
    market_place = FactoryGirl.create(:market_place, bairro: "ACLIMACAO")
    other_market_place = FactoryGirl.create(:market_place, bairro: "IPIRANGA")

    search_parameters = { market_place: { bairro: "ACLIMACAO" } }

    get :index, params: search_parameters

    json = JSON.parse(response.body)

    expect(response.body).to include market_place.to_json
    expect(response.body).to_not include other_market_place.to_json
  end

  # all parameters together
  it "returns the market_places that have the search params provided" do
    market_place = FactoryGirl.create(:market_place, distrito: "VILA MARIANA", regiao5: "Sul", nome_feira: "Feira 1", bairro: "ACLIMACAO")
    other_market_place = FactoryGirl.create(:market_place, distrito: "VILA MARIANA", regiao5: "Sul", nome_feira: "Feira 2", bairro: "IPIRANGA")

    search_parameters = { market_place: { distrito: "VILA MARIANA", regiao5: "Sul", nome_feira: "Feira 1", bairro: "ACLIMACAO" } }

    get :index, params: search_parameters

    json = JSON.parse(response.body)

    expect(response.body).to include market_place.to_json
    expect(response.body).to_not include other_market_place.to_json
  end
end

#################################
##### Testing CREATE action #####
#################################

RSpec.describe Api::MarketPlacesController, "create" do
  it "should create a market_place" do
    create_params = { market_place: FactoryGirl.attributes_for(:market_place) }

    expect{
      post :create, params: create_params
      }.to change(MarketPlace, :count).by(1)

    expect(response).to have_http_status :created
  end

  it "should respond :forbidden if the ID already exists and returns an error message" do
    FactoryGirl.create(:market_place, id: 1)

    create_params = { market_place: FactoryGirl.attributes_for(:market_place, id: 1) }

    post :create, params: create_params

    json = JSON.parse(response.body)

    expect(response).to have_http_status :forbidden

    expect(json.has_key?('error')).to eq true
  end

  it "should respond :unprocessable_entity if the market_place couldn't be created" do
    allow(MarketPlace).to receive(:create).and_return(false)

    post :create

    expect(response).to have_http_status :unprocessable_entity
  end
end

#################################
##### Testing UPDATE action #####
#################################

RSpec.describe Api::MarketPlacesController, "update" do
  it "should update a market_place" do
    market_place = FactoryGirl.create(:market_place, id: 1)

    update_params = { id: 1, market_place: { distrito: "VILA MARIANA", regiao5: "Sul" } }

    allow(MarketPlace).to receive(:find).and_return(market_place)
    expect(market_place).to receive(:update).with(hash_including(distrito: "VILA MARIANA", regiao5: "Sul")).and_return(true)

    patch :update, params: update_params

    expect(response).to have_http_status :ok
  end

  it "should respond :unprocessable_entity if market_place couldn't be updated" do
    market_place = FactoryGirl.create(:market_place, id: 1)

    allow(market_place).to receive(:update).and_return(false)
    allow(MarketPlace).to receive(:find).and_return(market_place)

    patch :update, params: { id: market_place }

    expect(response).to have_http_status :unprocessable_entity
  end

  it "doesn't update the ID attribute" do
    market_place = FactoryGirl.create(:market_place, id: 1)

    update_params = { id: 1, market_place: { id: 15, distrito: "VILA MARIANA", regiao5: "Sul" } }

    allow(MarketPlace).to receive(:find).and_return(market_place)

    patch :update, params: update_params

    expect(market_place.reload.id).to eq 1
  end

  it "should respond :not_found if the market_place doesn't exist" do
    market_place = FactoryGirl.create(:market_place, id: 10)

    patch :destroy, params: { id: 1 }

    expect(response).to have_http_status :not_found
  end
end

##################################
##### Testing DESTROY action #####
##################################

RSpec.describe Api::MarketPlacesController, "destroy" do
  it "should delete the market_place if it exists" do
    market_place = FactoryGirl.create(:market_place, id: 10)

    expect{
      delete :destroy, params: { id: market_place }
      }.to change(MarketPlace, :count).by(-1)

    expect(response).to have_http_status :ok
  end

  it "should respond :unprocessable_entity if the market_place can't be destroyed" do
    market_place = FactoryGirl.create(:market_place, id: 10)

    allow(market_place).to receive(:destroy).and_return(false)
    allow(MarketPlace).to receive(:find).and_return(market_place)

    delete :destroy, params: { id: market_place }

    expect(response).to have_http_status :unprocessable_entity
  end

  it "should respond :not_found if the market_place doesn't exist" do
    market_place = FactoryGirl.create(:market_place, id: 10)

    delete :destroy, params: { id: 1 }

    expect(response).to have_http_status :not_found
  end
end
