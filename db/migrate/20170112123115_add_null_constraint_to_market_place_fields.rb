class AddNullConstraintToMarketPlaceFields < ActiveRecord::Migration[5.0]
  def change
    change_column_null :market_places, :long, false
    change_column_null :market_places, :lat, false
    change_column_null :market_places, :setcens, false
    change_column_null :market_places, :areap, false
    change_column_null :market_places, :coddist, false
    change_column_null :market_places, :distrito, false
    change_column_null :market_places, :codsubpref, false
    change_column_null :market_places, :subprefe, false
    change_column_null :market_places, :regiao5, false
    change_column_null :market_places, :regiao8, false
    change_column_null :market_places, :nome_feira, false
    change_column_null :market_places, :registro, false
    change_column_null :market_places, :logradouro, false
  end
end
