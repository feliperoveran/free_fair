class CreateMarketPlaces < ActiveRecord::Migration[5.0]
  def change
    create_table :market_places do |t|
      t.integer :long
      t.integer :lat
      t.text :setcens
      t.text :areap
      t.integer :coddist
      t.text :distrito
      t.integer :codsubpref
      t.text :subprefe
      t.text :regiao5
      t.text :regiao8
      t.text :nome_feira
      t.text :registro
      t.text :logradouro
      t.text :numero
      t.text :bairro
      t.text :referencia

      t.timestamps
    end
  end
end
