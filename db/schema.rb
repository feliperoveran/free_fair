# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170112123115) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "market_places", force: :cascade do |t|
    t.integer  "long",       null: false
    t.integer  "lat",        null: false
    t.text     "setcens",    null: false
    t.text     "areap",      null: false
    t.integer  "coddist",    null: false
    t.text     "distrito",   null: false
    t.integer  "codsubpref", null: false
    t.text     "subprefe",   null: false
    t.text     "regiao5",    null: false
    t.text     "regiao8",    null: false
    t.text     "nome_feira", null: false
    t.text     "registro",   null: false
    t.text     "logradouro", null: false
    t.text     "numero"
    t.text     "bairro"
    t.text     "referencia"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
