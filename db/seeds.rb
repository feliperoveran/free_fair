require 'csv'

puts "Importando registros..."

file = Rails.root.join('lib', 'DEINFO_AB_FEIRASLIVRES_2014.csv')

CSV.foreach(file, headers: true, header_converters: :symbol) do |row|
  # initializing and then saving so we can set the id we want (ofc duplicates won't be allowed)
  market_place = MarketPlace.new(row.to_hash)

  market_place.save!
end

puts "Foram criadas #{MarketPlace.count} feiras."
